<?php

/**
 *
 * @file Inserts geolocation information into HTTP headers to these values can be used in other module, plugins, and themes.
 *
 * IP2Location is a non-intrusive geo IP solution to help you to identify
 * visitor's geographical location, i.e. country, region, city, latitude,
 * longitude, ZIP code, time zone, connection speed, ISP and domain name,
 * IDD country code, area code, weather station code and name, and mobile
 * carrier, elevation, usage type information using a proprietary IP
 * address lookup database and technology without invading the Internet
 * user's privacy.
 *
 * This module is using IP2Location database to lookup for visitor
 * location and insert the variables into HTTP headers. Other developers
 * can use these variables to customize their content based on visitor
 * location.
 * @author Hexasoft.    <https://www.drupal.org/u/hexasoft>
 */

/******************************************************************************
 * Drupal Hooks                                 *
 ******************************************************************************/

/**
 * Implements hook_help().
 */
function ip2location_help($path = 'admin/help#ip2location', $arg = NULL) {
  if ($path == 'admin/help#ip2location') {
    return t('Enables geolocation information in server headers.');
  }
}

/**
 * Implements hook_permission().
 */
function ip2location_permission() {
  return array('administer ip2location' => array());
}

/**
 * Implements hook_menu().
 */
function ip2location_init() {
  // unset($_SESSION['ip2location']);
  if (!isset($_SESSION['ip2location'])) {
    $_SESSION['ip2location'] = json_encode(ip2location_get_location());
  }
  ip2location_set_headers();
}

/**
 * Implements hook_menu().
 */
function ip2location_menu() {
  $items = array();

  $items['admin/config/people/ip2location'] = array(
    'title' => 'IP2Location',
    'description' => 'Configure the IP2Location settings',
    'access arguments' => array('administer ip2location'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ip2location_admin_settings'),
    'file' => 'includes/ip2location.admin.inc',
  );
  return $items;
}


/******************************************************************************
 * Module Functions                               *
 ******************************************************************************/


/**
 * Gets location information from the IP address.
 *
 * @param string $ip
 *   IP address as a dotted quad string (e.g. "127.0.0.1").
 *
 * @return array|null
 *   Array of location information.
 */
function ip2location_get_location($ip = '') {
  $ip = ($ip) ? $ip : ip_address();
  $result = array();

  if (!file_exists(variable_get('ip2location_bin_path', ''))) {
    return NULL;
  }

  module_load_include('inc', 'ip2location', 'includes/IP2Location');

  switch (variable_get('ip2location_cache', '')) {
    case 'memory_cache':
      $data = new IP2Location(variable_get('ip2location_bin_path', ''), IP2Location::MEMORY_CACHE);
      break;

    case 'shared_memory':
      $data = new IP2Location(variable_get('ip2location_bin_path', ''), IP2Location::SHARED_MEMORY);
      break;

    default:
      $data = new IP2Location(variable_get('ip2location_bin_path', ''), IP2Location::FILE_IO);
  }

  $record = $data->lookup($ip, IP2Location::ALL);

  if ($record) {
    foreach ($record as $key => $value) {
      $result[$key] = (in_array($key, array(
        'countryName',
        'regionName',
        'cityName',
      ))) ? ip2location_fix_case($value) : $value;
    }
  }

  return $result;
}

/**
 * Fix wording case.
 *
 * @param string $s
 *   Any string.
 *
 * @return string
 *   Formatted string.
 */
function ip2location_fix_case($s) {
  $s = ucwords(strtolower($s));
  $s = preg_replace_callback("/([a-zA-Z]{1}')([a-zA-Z0-9]{1})/s", create_function('$matches', 'return $matches[1].strtoupper($matches[2]);'), $s);
  return $s;
}

/**
 * Retrieves custom HTTP headers.
 */
function ip2location_set_headers() {
  if (!isset($_SESSION['ip2location'])) {
    return NULL;
  }

  if (is_null($data = json_decode($_SESSION['ip2location'])) === FALSE) {
    $headers = array(
      'ipAddress' => 'IP2LOCATION_IP_ADDRESS',
      'countryCode' => 'IP2LOCATION_COUNTRY_CODE',
      'countryName' => 'IP2LOCATION_COUNTRY_NAME',
      'regionName' => 'IP2LOCATION_REGION_NAME',
      'cityName' => 'IP2LOCATION_CITY_NAME',
      'latitude' => 'IP2LOCATION_LATITUDE',
      'longitude' => 'IP2LOCATION_LONGITUDE',
      'isp' => 'IP2LOCATION_ISP',
      'domainName' => 'IP2LOCATION_DOMAIN_NAME',
      'zipCode' => 'IP2LOCATION_ZIP_CODE',
      'timeZone' => 'IP2LOCATION_TIME_ZONE',
      'netSpeed' => 'IP2LOCATION_NET_SPEED',
      'iddCode' => 'IP2LOCATION_IDD_CODE',
      'areaCode' => 'IP2LOCATION_AREA_CODE',
      'weatherStationCode' => 'IP2LOCATION_WEATHER_STATION_CODE',
      'weatherStationName' => 'IP2LOCATION_WEATHER_STATION_NAME',
      'mcc' => 'IP2LOCATION_MCC',
      'mnc' => 'IP2LOCATION_MNC',
      'mobileCarrierName' => 'IP2LOCATION_CARRIER_NAME',
      'elevation' => 'IP2LOCATION_ELEVATION',
      'usageType' => 'IP2LOCATION_USAGE_TYPE',
    );

    foreach ($data as $key => $value) {
      if (isset($headers[$key])) {
        drupal_add_http_header($headers[$key], $value);
      }
    }
  }
}

/**
 * Retrieves custom HTTP headers by key.
 *
 * @param string $key
 *   An identifier to retrieve value of header.
 *
 * @return string
 *   The value of header with specified key.
 */
function ip2location_get_header($key) {
  return drupal_get_http_header($key);
}
